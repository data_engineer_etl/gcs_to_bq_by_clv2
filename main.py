from google.cloud import bigquery

def hello_gcs(event, context):
    # Définissez les variables pour le projet, dataset et table
    project_id = "enduring-tea-423217-k5"
    bq_dataset = "Dataset_tuto_1"
    bq_table = "table_tuto2"

    # Construction du chemin complet de la table dans BigQuery
    table_id = f"{project_id}.{bq_dataset}.{bq_table}"

    # Récupération du nom du fichier et du nom du bucket à partir de l'événement
    file_name = event['name']
    bucket_name = event['bucket']
    gcs_uri = f"gs://{bucket_name}/{file_name}"

    # Création du client BigQuery
    client = bigquery.Client()

    # Configuration du job de chargement
    job_config = bigquery.LoadJobConfig(
        schema=[
            bigquery.SchemaField("first_name", "STRING"),
            bigquery.SchemaField("last_name", "STRING"),
            bigquery.SchemaField("age", "INT64")
        ],
        skip_leading_rows=1,  # Pour sauter l'en-tête du fichier CSV
        source_format=bigquery.SourceFormat.CSV
    )

    # Déclenchement du job de chargement
    load_job = client.load_table_from_uri(
        gcs_uri, table_id, job_config=job_config
    )

    # Attendre la fin du job de chargement
    load_job.result()
